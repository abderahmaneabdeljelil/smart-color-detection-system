import numpy as np
import cv2
colors={"Rouge1":[0,20],
        "Orange":[20,40],
        "Jaune":[40,80],
        "Vert":[80,160],
        "Cyan":[160,200],
        "Bleu":[200,260],
        "Magenta":[260,330],
        "Rouge2":[330,360]
        }
def detect_couleur(image,cnt,colors):
    image_hsv=cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
    mask = np.zeros(image_hsv.shape[:2], dtype="uint8")
    cv2.drawContours(mask, [cnt], -1, 255, -1)
    mask = cv2.erode(mask, None, iterations=2)
    mean = cv2.mean(image_hsv, mask=mask)
    if image.dtype=='uint8':
        for color in colors:
            if 2*mean[0] in range(colors[color][0],colors[color][1]):
                return(color)
    else:
        for color in colors:
            if mean[0] in range(colors[color][0],colors[color][1]):
                return(color)
image = cv2.imread('2017-10-12_09-58-25.jpg')
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(gray,250,255,cv2.THRESH_BINARY_INV)
img,contours,h = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
for cnt in contours:
    perimetre=cv2.arcLength(cnt,True)
    approx = cv2.approxPolyDP(cnt,0.01*perimetre,True)
    color = detect_couleur(image,cnt,colors)
    M = cv2.moments(cnt)
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
    cv2.drawContours(image,[cnt],-1,(0,255,0),2)
    if len(approx)==3:
        shape = "triangle"
    elif len(approx)==4:
        (x, y, w, h) = cv2.boundingRect(approx)
        ratio = w / float(h)
        if ratio >= 0.95 and ratio <= 1.05:
            shape = "carre"
        else:
            shape = "rectangle"
    elif len(approx)==5:
        shape = "pentagone"
    elif len(approx)==6:
        shape = "hexagone"
    else:
        shape= "cercle"
    cv2.putText(image, shape + " " + color, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,0.5, (125, 125, 125), 2)
cv2.imshow('image',image)
cv2.waitKey(5000)
cv2.imwrite('result.jpg',image)
cv2.destroyAllWindows()
