import sys
from tkinter import *
import tkFileDialog
from tkFileDialog import askopenfilename
import subprocess
from subprocess import Popen
import os

fasta= ""
lista = ""
window = Tk()
window.title("Sequence extractor")

def callback():
    fasta= askopenfilename()
    lista = askopenfilename()

    print ('fasta')
    print ('lista')
    return fasta,lista

errmsg = 'Error!'
but_fas = Button(text='FASTA Open', command=callback).pack(fill=X)
but_lis = Button(text='Lista Open', command=callback).pack(fill=X)

subprocess.call(['c:\Python_programs\Seq_extractor.py', '-l', lista, '-f', fasta])

b = Button(text = 'execute!', command=callback ).pack(fill=X)
