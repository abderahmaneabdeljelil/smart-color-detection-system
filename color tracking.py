import cv2
import numpy as np
from statistics import mean

cap=cv2.VideoCapture(0)
while(1):
    _, img=cap.read()
    hsv=cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    #red_color
    red_lower=np.array([136,87,111],np.uint8)
    red_upper=np.array([180,255,255],np.uint8)

    #pink_color
    pink_lower=np.array([328,235,255],np.uint8)
    pink_upper=np.array([350,63,255],np.uint8)

    #Purple_color
    purple_lower=np.array([328,235,255],np.uint8)
    purple_upper=np.array([75,0,130],np.uint8)
    
     #gray_color
    gray_lower=np.array([0,5,50],np.uint8)
    gray_upper=np.array([360,50,255],np.uint8)
    #black_color
    black_lower=np.array([0,0,0],np.uint8)
    black_upper=np.array([50,50,100],np.uint8)
    #green_color
    green_lower=np.array([57,88,90],np.uint8)
    green_upper=np.array([68,242,231],np.uint8)
    # orange_color
    orange_lower = np.array([1, 190, 200], np.uint8)
    orange_upper = np.array([18, 255, 255], np.uint8)

     # brown_color
    brown_lower = np.array([10, 100, 20], np.uint8)
    brown_upper = np.array([20, 255, 200], np.uint8)
    
    #blue_color
    blue_lower=np.array([99,115,150],np.uint8)
    blue_upper=np.array([110,255,255],np.uint8)
    #yellow_color
    yellow_lower=np.array([22,60,200],np.uint8)
    yellow_upper=np.array([60,255,255],np.uint8)

    #white_color
    white_lower=np.array([0, 0, 200],np.uint8)
    white_upper=np.array([145, 60, 255],np.uint8)

    red=cv2.inRange(hsv, red_lower, red_upper)
    purple=cv2.inRange(hsv, purple_lower, purple_upper)
    pink=cv2.inRange(hsv, pink_lower, pink_upper)
    blue=cv2.inRange(hsv, blue_lower, blue_upper)
    yellow=cv2.inRange(hsv, yellow_lower, yellow_upper)
    orange = cv2.inRange(hsv, orange_lower, orange_upper)
    brown = cv2.inRange(hsv, brown_lower, brown_upper)
    white = cv2.inRange(hsv, white_lower, white_upper)
    green = cv2.inRange(hsv, green_lower, green_upper)
    black = cv2.inRange(hsv, black_lower, black_upper)
    gray = cv2.inRange(hsv, gray_lower, gray_upper)

    kernal = np.ones((5,5), "uint8")
    
    red=cv2.dilate(red, kernal)
    res=cv2.bitwise_and(img, img, mask=red)
                
    purple=cv2.dilate(purple, kernal)
    res9=cv2.bitwise_and(img, img, mask=purple)

    pink=cv2.dilate(pink, kernal)
    res8=cv2.bitwise_and(img, img, mask=pink)

    gray=cv2.dilate(red, kernal)
    res7=cv2.bitwise_and(img, img, mask=gray)

    green=cv2.dilate(green, kernal)
    res5=cv2.bitwise_and(img, img, mask=green)

    blue=cv2.dilate(blue, kernal)
    res1=cv2.bitwise_and(img, img, mask=blue)
    resl1= res1[1]
##    L= resl1.shape[0]
##    l =resl1.shape[1]
##    H=[]
##    S=[]
##    V=[]
##    for x in range(L):
##        for y in range(l):
##            if (resl1[x][y]!=0):
##                h,s,v=resl1[x][y]
##                H.append(r)
##                S.append(s)
##                V.append(v)
##            else:
##                pass
##    hmoy= np.mean(H)
##    smoy= np.mean(S)
##    vmoy= np.mean(V)
##    
##    
    yellow=cv2.dilate(yellow, kernal)
    res2=cv2.bitwise_and(img, img, mask=yellow)

    orange = cv2.dilate(orange, kernal)
    res3 = cv2.bitwise_and(img, img, mask=orange)

    brown = cv2.dilate(brown, kernal)
    res10 = cv2.bitwise_and(img, img, mask=brown)

    white = cv2.dilate(white, kernal)
    res4 = cv2.bitwise_and(img, img, mask=white)

    black = cv2.dilate(black, kernal)
    res6 = cv2.bitwise_and(img, img, mask=black)

    (_,contours,hierarchy)=cv2.findContours(red,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
            cv2.putText(img,"RED COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,255))

    (_,contours,hierarchy)=cv2.findContours(pink,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
            cv2.putText(img,"PINK COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,255))
    (_,contours,hierarchy)=cv2.findContours(gray,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(128,128,128),2)
            cv2.putText(img,"GRAY COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (128,128,128))    
        
    (_,contours,hierarchy)=cv2.findContours(black,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
            cv2.putText(img,"Black COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,0))


    (_,contours,hierarchy)=cv2.findContours(purple,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)
            cv2.putText(img,"Purple COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,0))
    (_,contours,hierarchy)=cv2.findContours(white,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,255,255),2)
            cv2.putText(img,"white COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,255,255))
            
    (_,contours,hierarchy)=cv2.findContours(green,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
            cv2.putText(img,"GREEN COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,255,0))

    (_, contours, hierarchy) = cv2.findContours(orange, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if (area > 300):
            x, y, w, h = cv2.boundingRect(contour)
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
            cv2.putText(img, "ORANGE COLOR", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255))

    (_,contours,hierarchy)=cv2.findContours(blue,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    (_, contours, hierarchy) = cv2.findContours(brown, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if (area > 300):
            x, y, w, h = cv2.boundingRect(contour)
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
            cv2.putText(img, "Brown COLOR", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255))
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
            cv2.putText(img,"BLUE COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,0,0))

    (_,contours,hierarchy)=cv2.findContours(yellow,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(60,245,255),2)
            cv2.putText(img,"YELLOW COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (60,245,255))
    cv2.imshow("Color Tracking",img)
    cv2.imshow("Color Tracking_HSV",img)
    cv2.imshow("mask",red)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        cap.release()
        cv2.destroyAllWindows()
        break

