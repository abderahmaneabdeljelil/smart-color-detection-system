import numpy as np
import cv2

cap = cv2.VideoCapture(0) # video capture source camera (Here webcam of laptop) 
ret,frame = cap.read() # return a single frame in variable `frame`
##HSV=cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
##red_lower=np.array([136,87,111],np.uint8)
##red_upper=np.array([180,255,255],np.uint8)
##red=cv2.inRange(hsv, red_lower, red_upper)
##kernal = np.ones((5,5), "uint8")
##red=cv2.dilate(red, kernal)
##res=cv2.bitwise_and(img, img, mask=red)
while(1):
    _, img=cap.read()
    hsv=cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    #red_color
    red_lower=np.array([136,87,111],np.uint8)
    red_upper=np.array([180,255,255],np.uint8)
    # orange_color
    orange_lower = np.array([1, 190, 200], np.uint8)
    orange_upper = np.array([18, 255, 255], np.uint8)
    #blue_color
    blue_lower=np.array([99,115,150],np.uint8)
    blue_upper=np.array([110,255,255],np.uint8)
    #yellow_color
    yellow_lower=np.array([22,60,200],np.uint8)
    yellow_upper=np.array([60,255,255],np.uint8)

    #white_color
    white_lower=np.array([0, 0, 200],np.uint8)
    white_upper=np.array([145, 60, 255],np.uint8)

    red=cv2.inRange(hsv, red_lower, red_upper)
    blue=cv2.inRange(hsv, blue_lower, blue_upper)
    yellow=cv2.inRange(hsv, yellow_lower, yellow_upper)
    orange = cv2.inRange(hsv, orange_lower, orange_upper)
    white = cv2.inRange(hsv, white_lower, white_upper)

    kernal = np.ones((5,5), "uint8")
    
    red=cv2.dilate(red, kernal)
    res=cv2.bitwise_and(img, img, mask=red)

    blue=cv2.dilate(blue, kernal)
    res1=cv2.bitwise_and(img, img, mask=blue)
    
    yellow=cv2.dilate(yellow, kernal)
    res2=cv2.bitwise_and(img, img, mask=yellow)

    orange = cv2.dilate(orange, kernal)
    res3 = cv2.bitwise_and(img, img, mask=orange)

    white = cv2.dilate(white, kernal)
    res4 = cv2.bitwise_and(img, img, mask=white)

    (_,contours,hierarchy)=cv2.findContours(white,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,255,255),2)
            cv2.putText(img,"white COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,255))

    (_,contours,hierarchy)=cv2.findContours(red,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
            cv2.putText(img,"RED COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,255))

    (_, contours, hierarchy) = cv2.findContours(orange, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if (area > 300):
            x, y, w, h = cv2.boundingRect(contour)
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
            cv2.putText(img, "ORANGE COLOR", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255))

    (_,contours,hierarchy)=cv2.findContours(blue,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
            cv2.putText(img,"BLUE COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,0,0))

    (_,contours,hierarchy)=cv2.findContours(yellow,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area>300):
            x,y,w,h = cv2.boundingRect(contour)
            img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
            cv2.putText(img,"YELLOW COLOR",(x,y),cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,255,0))
    cv2.imshow("Color Tracking",img)
    cv2.imshow("red_mask",res)
    cv2.imshow("white_mask",res4)
    cv2.imshow("blue_mask",res1)
    cv2.imshow("yellow_mask",res2)
    cv2.imshow("orange_mask",res3)
##  cv2.imshow("Color Tracking_HSV",hsv)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        cap.release()
        cv2.destroyAllWindows()
        break


    cv2.imshow('img1',frame) #display the captured image
    if cv2.waitKey(1) & 0xFF == ord('y'): #save on pressing 'y' 
        cv2.imwrite('images/c1.png',frame)
        cv2.destroyAllWindows()
        break

cap.release()
##import cv2
##cap = cv2.VideoCapture(0)
##cap.set(3,640) #width=640
##cap.set(4,480) #height=480
##
##if cap.isOpened():
##    _,frame = cap.read()
##    cap.release() #releasing camera immediately after capturing picture
##    if _ and frame is not None:
##        cv2.imwrite('img.jpg', frame)
